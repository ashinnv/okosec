module okosec

go 1.16

require (
	gitlab.com/ashinnv/okolog v0.0.0-20210916124216-958d7d3b1cee
	golang.org/x/crypto v0.0.0-20210915214749-c084706c2272
)
